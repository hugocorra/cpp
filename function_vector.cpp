#include <algorithm>
#include <iostream>
#include <boost/fusion/container/generation/make_vector.hpp>
#include <boost/fusion/include/make_vector.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <tuple>


class MyClass
{
public:
    int getInt() { return 10; }
    float getFloat() { return 10.0; }
    std::string getString() { return "Hello"; }

private:
    
};

template <typename O>
struct call
{
    call(O o):
        obj(o)
    {
    }

    template <typename T>
    void operator()(T& t) const
    {
        std::cout << "teste" << t(obj) << std::endl;
        //t();
    }

    O obj;
};


/*
template <typename T>
void call_func(T t)
{
    std::cout << "..." << t() << std::endl;
}

int teste()
{
    std::cout << "teste" << std::endl;
}
*/

/*
template <typename T>
void apply(T t)
{
    std::cout << "> " << std::endl;
}

template <typename T, typename... U>
void apply(T t, U... u)
{
    apply(t);
    apply(u...);
}

*/

int main()
{
    MyClass c;
    
  /*
    auto x = make_tuple(std::mem_fn(&MyClass::getInt), std::mem_fn(&MyClass::getFloat), std::mem_fn(&MyClass::getString));
    apply(tie(x));    
    */

    auto x = boost::fusion::make_vector(std::mem_fn(&MyClass::getInt), std::mem_fn(&MyClass::getFloat));
    boost::fusion::for_each(x, call<MyClass>(c));

    return 0;
}

